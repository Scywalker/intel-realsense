﻿//--------------------------------------------------------------------------------------
// Copyright 2015 Intel Corporation
// All Rights Reserved
//
// Permission is granted to use, copy, distribute and prepare derivative works of this
// software for any purpose and without fee, provided, that the above copyright notice
// and this statement appear in all copies.  Intel makes no representations about the
// suitability of this software for any purpose.  THIS SOFTWARE IS PROVIDED "AS IS."
// INTEL SPECIFICALLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, AND ALL LIABILITY,
// INCLUDING CONSEQUENTIAL AND OTHER INDIRECT DAMAGES, FOR THE USE OF THIS SOFTWARE,
// INCLUDING LIABILITY FOR INFRINGEMENT OF ANY PROPRIETARY RIGHTS, AND INCLUDING THE
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  Intel does not
// assume any responsibility for any errors which may appear in this software nor any
// responsibility to update it.
//--------------------------------------------------------------------------------------
using System;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO;
using System.Drawing;
using System.Threading;

namespace CameraStreams
{
    public partial class MainWindow : Window
    {
        private PXCMSession session;
        private PXCMSenseManager senseManager;
        private Thread update;

        public MainWindow()
        {
            InitializeComponent();

            // Configure RealSense session and SenseManager interface
            session = PXCMSession.CreateInstance();
            senseManager = session.CreateSenseManager();
            senseManager.EnableStream(PXCMCapture.StreamType.STREAM_TYPE_LEFT, 320, 240, 30);
            senseManager.EnableStream(PXCMCapture.StreamType.STREAM_TYPE_RIGHT, 320, 240, 30);
            senseManager.EnableStream(PXCMCapture.StreamType.STREAM_TYPE_COLOR, 320, 240, 30);
            senseManager.EnableStream(PXCMCapture.StreamType.STREAM_TYPE_DEPTH, 320, 240, 30);
            senseManager.Init();

            // Start Update thread
            update = new Thread(new ThreadStart(Update));
            update.Start();
        }

        private void Update()
        {
            // Start AcquireFrame-ReleaseFrame loop
            while (senseManager.AcquireFrame(true) >= pxcmStatus.PXCM_STATUS_NO_ERROR)
            {
                PXCMCapture.Sample sample = senseManager.QuerySample();
 
                // Get Left IR image data
                PXCMImage.ImageData irLeftData;
                Bitmap irLeftBitmap;
                sample.left.AcquireAccess(PXCMImage.Access.ACCESS_READ, PXCMImage.PixelFormat.PIXEL_FORMAT_RGB32, out irLeftData);
                irLeftBitmap = irLeftData.ToBitmap(0, sample.left.info.width, sample.left.info.height);

                // Get Right IR image data
                PXCMImage.ImageData irRightData;
                Bitmap irRightBitmap;
                sample.right.AcquireAccess(PXCMImage.Access.ACCESS_READ, PXCMImage.PixelFormat.PIXEL_FORMAT_RGB32, out irRightData);
                irRightBitmap = irRightData.ToBitmap(0, sample.right.info.width, sample.right.info.height);

                // Get Color image data
                PXCMImage.ImageData colorData;
                Bitmap colorBitmap;
                sample.color.AcquireAccess(PXCMImage.Access.ACCESS_READ, PXCMImage.PixelFormat.PIXEL_FORMAT_RGB32, out colorData);
                colorBitmap = colorData.ToBitmap(0, sample.color.info.width, sample.color.info.height);

                // Get Depth image data
                PXCMImage.ImageData depthData;
                Bitmap depthBitmap;
                sample.depth.AcquireAccess(PXCMImage.Access.ACCESS_READ, PXCMImage.PixelFormat.PIXEL_FORMAT_RGB32, out depthData);
                depthBitmap = depthData.ToBitmap(0, sample.depth.info.width, sample.depth.info.height);

                // Update UI
                Render(irLeftBitmap, irRightBitmap, colorBitmap, depthBitmap);
          
                // Release resources
                irLeftBitmap.Dispose();
                sample.left.ReleaseAccess(irLeftData);

                irRightBitmap.Dispose();
                sample.right.ReleaseAccess(irRightData);

                colorBitmap.Dispose();
                sample.color.ReleaseAccess(colorData);

                depthBitmap.Dispose();
                sample.depth.ReleaseAccess(depthData);

                // Release frame       
                senseManager.ReleaseFrame();
            }
        }

        private void Render(Bitmap bitmapLeft, Bitmap bitmapRight, Bitmap bitmapColor, Bitmap bitmapDepth)
        {
            BitmapImage bitmapImageLeft = ConvertBitmap(bitmapLeft);
            BitmapImage bitmapImageRight = ConvertBitmap(bitmapRight);
            BitmapImage bitmapImageColor = ConvertBitmap(bitmapColor);
            BitmapImage bitmapImageDepth = ConvertBitmap(bitmapDepth);

            // Update the WPF Image control
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(delegate()
            {
                imgLeftStream.Source = bitmapImageLeft;
                imgRightStream.Source = bitmapImageRight;
                imgColorStream.Source = bitmapImageColor;
                imgDepthStream.Source = bitmapImageDepth;
            }));
        }

        private BitmapImage ConvertBitmap(Bitmap bitmap)
        {
            BitmapImage bitmapImage = null;
            
            if (bitmap != null)
            {
                MemoryStream memoryStream = new MemoryStream();
                bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Bmp);
                memoryStream.Position = 0;
                bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memoryStream;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();
            }

            return bitmapImage;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ShutDown();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            ShutDown();
            this.Close();
        }

        private void ShutDown()
        {
            // Stop the Update thread
            update.Abort();

            // Dispose RealSense objects
            senseManager.Dispose();
            session.Dispose();
        }
    }
}
