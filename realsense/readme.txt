APPLICABLE LICENSES
See license.txt contained in this zip file.

DESCRIPTION
This code sample demonstrates the basics of capturing and viewing raw R200 camera streams in C#/XAML using the Intel� RealSense� SDK for Windows*. The Visual Studio* solution is comprised of four projects:
 - ColorStream � Displays the color stream from the RGB camera
 - DepthStream � Displays the depth stream
 - IRStreams � Displays the left and right IR camera streams
 - AllStreams � Shows all of the above in a single window

NOTES
1. This project uses an explicit path to libpxcclr.cs.dll (the managed RealSense DLL): C:\Program Files (x86)\Intel\RSSDK\bin\x64. This reference will need to be changed if your installation path is different.

2. To build and run a particular project, right-click on the project name (e.g., AllStreams) in Solution Explorer, then select Set as StartUp Project from the menu options.

Prepared by: Bryan Brown
http://software.intel.com/realsense

*Other names and brands may be claimed as the property of others
� 2015 Intel Corporation.
